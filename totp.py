import pyotp
import hashlib
import base64

secret = bytes("secret", "utf-8")
unixtimestamp = 1
secret = base64.b32encode(secret)

# getting totp code for specific day
totp = pyotp.TOTP(secret, 10, hashlib.sha512)