# TOTP Encryption
In this project I am trying to get a encryption that is based on TOTP, Base64 and AES256, it should work like the following:
First you make a TOTP secret and search for a date which you want, then you input them into the CMD where the TOTP-code for that time
will get encoded by Base64, which is then building the key for the AES256 encryption key.

The good thing about this is, that you can use the same TOTP secret multiple times, but for different dates.
