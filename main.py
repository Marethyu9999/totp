# Importing modules
import base64
import pyotp
import Cryptodome
from Cryptodome.Cipher import AES
import hashlib

def aes_encryption(key1, salt1, secret):
    salt = base64.b64encode(bytes(salt1, "utf-8"))
    key = hashlib.scrypt(key1, salt=salt, n=2**14, r=8, p=1, dklen=32)
    cipher = AES.new(key, AES.MODE_GCM)
    ciphertext = cipher.encrypt(bytes(secret, "utf-8"))
    return ciphertext

def aes_decryption(key1, salt1, cptext):
    salt = base64.b64encode(bytes(salt1, "utf-8"))
    key = hashlib.scrypt(key1, salt=salt, n=2**14, r=8, p=1, dklen=32)
    cipher = AES.new(key, AES.MODE_GCM)
    ciphertext = cipher.decrypt(bytes(cptext, "utf-8"))
    return ciphertext


def encrypt():
    # collecting inputs for totp secret, unixtimestamp and text to encode
    secret = base64.b32encode(bytes(input("TOTP secret: "), "utf-8"))
    unixtimestamp = input("Unix Timestamp: ")
    to_encode = input("Encode: ")

    # getting totp code for specific day
    totp = pyotp.TOTP(secret, 10, hashlib.sha512)
    totp_code = bytes(totp.at(int(unixtimestamp)), "utf-8")

    # making aes256 password out of base64 encoded totp code
    return aes_encryption(totp_code, unixtimestamp, to_encode)

def decrypt():
    aes_encrypt = input("Encrypted text: ")
    unixtimestamp = input("Unix Timestamp: ")
    secret = base64.b32encode(bytes(input("TOTP secret: "), "utf-8"))

    totp = pyotp.TOTP(secret, 10, hashlib.sha512)
    totp_code = bytes(totp.at(int(unixtimestamp)), "utf-8")
    
    return aes_decryption(totp_code, unixtimestamp, aes_encrypt)

def main():
    print("Choice\n[1]: Encrypt         [2]: Decrypt")
    choice = int(input("Choice: "))
    if choice == 1:
        print(encrypt())
    elif choice == 2:
        print(decrypt())
    else:
        print("This option isn't existing!!!")
    input()


if __name__ == "__main__":
    main()
